# ifndef __SENDER_H_
# define __SENDER_H_

/* Function Prototyping */
int sendNotDeadPacket (unsigned int *nSequenceNumber,FILE *fpWriteFile);
int sendBroadcastPacket (unsigned int *nSequenceNumber,FILE *fpWriteFile);
int createNotDeadFrame (char **strBufferNotDead, unsigned int nSequenceNumber);
int takeActionDependOnFile(unsigned int *nSequenceNumber);

# endif

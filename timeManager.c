# include "network.h"

int main () {
   unsigned int nMacTime [100];
   int nSocket_fd_DeleteMac,nSequenceNumber = 1000;
   int nIndexOfDecreaseTime = 0,nIndexOfstrMacTime = 0,nIndexOfMacTime = 0,nSizeOfMacTime = 0,nIndexOfCheckMac = 0;
   char strMacTime [100][16], strBuffer[16],strBufferDeleteMac [PACKET_LENGTH] ,*strSequenceChar;
   FILE * fp_Time_Info;
   struct sockaddr_ll sockaddr_ll_DeleteMac;
   int i;

   strBuffer[0]=SEND_SELF_PACKET;
   strBuffer[1]='\0';
   strSequenceChar = (char *) malloc (sizeof (char) * ETH_ALEN);
   if (strSequenceChar == NULL) {
      fprintf (stderr,"Memory Allocation Failed\n");
      return -1;
   }

   nSocket_fd_DeleteMac = socket (AF_PACKET,SOCK_DGRAM,htons (ETH_P_ALL));
   if (nSocket_fd_DeleteMac == -1) {
      fprintf (stderr,"%d %s\n",errno,strerror(errno));
      return -1;
   }
   createSockStructure(&sockaddr_ll_DeleteMac,nSocket_fd_DeleteMac,strBuffer);
   while (1) {
      if( access ("TIME_INFO.txt",F_OK) == 0) {
TIME :
         if ((fp_Time_Info = fopen ("TIME_INFO.txt","r")) != NULL) {
            fgets (strBuffer,16,fp_Time_Info);
            fclose (fp_Time_Info);
            unlink ("TIME_INFO.txt");
            for  (nIndexOfCheckMac = 0; nIndexOfCheckMac < nSizeOfMacTime; nIndexOfCheckMac++) {
               if (strcmp (strBuffer,strMacTime [nIndexOfCheckMac]) == 0) {
                  nMacTime [nIndexOfCheckMac] = /*60 */(TIME_ONE_MIN + TIME_TEN_SEC) ;
                  goto DONT_WRITE_MAC;
               }
            }
            strcpy (strMacTime [nIndexOfstrMacTime],strBuffer);
            nIndexOfstrMacTime++;
            nMacTime [nIndexOfMacTime] = /*60 */(TIME_ONE_MIN + TIME_TEN_SEC) ;
            nIndexOfMacTime++;
            nSizeOfMacTime++;  
         }
DONT_WRITE_MAC :
         for (nIndexOfDecreaseTime  = 0; nIndexOfDecreaseTime  < nSizeOfMacTime; nIndexOfDecreaseTime ++) {
            nMacTime [nIndexOfDecreaseTime ] = nMacTime [nIndexOfDecreaseTime ] - 1;
            if (nMacTime [nIndexOfDecreaseTime] == 0) {
               strBufferDeleteMac [0] = DELETE_MAC ;
               integerToCharacter (nSequenceNumber,&strSequenceChar);
               strBufferDeleteMac [1] = strSequenceChar[0];
               strBufferDeleteMac [2] = strSequenceChar[1];
               strBufferDeleteMac [3] = strSequenceChar[2];
               strBufferDeleteMac [4] = strSequenceChar[3];
               /*strncpy (strBufferDeleteMac + 1, strSequenceChar,4);*/ 
               strcpy (strBufferDeleteMac + 5,strMacTime[nIndexOfDecreaseTime]);
               /*for(i=5;i< (MAC_ALEN_CHAR+5);i++)
                 strBufferDeleteMac[i]=strBuffer[i-5];*/
               for(i=5;i< (MAC_ALEN_CHAR+5);i++)     
                  fprintf(stdout,"%c",strBufferDeleteMac[i]);
               fprintf(stdout,"\n");
               if ((sendto (nSocket_fd_DeleteMac, strBufferDeleteMac,(size_t) PACKET_LENGTH,0, (struct sockaddr *) &sockaddr_ll_DeleteMac, (socklen_t) sizeof (sockaddr_ll_DeleteMac))) == -1) {
                  fprintf (stderr,"%d %s\n",errno,strerror (errno));
                  return -1;
               }
               nSequenceNumber++;
            }
         }
           goto TIME;
      }
   }
   return 0;
}

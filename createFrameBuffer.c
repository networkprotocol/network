# include "network.h"

int createFrameBuffer (char **strReturnBuffer,char strBuffer[PACKET_LENGTH],char strSourceMac[MAC_ALEN_CHAR],unsigned int nSequenceNumber) {
   int i,j;
   char *strBufferTemp,*strSequenceChar;

   strBufferTemp = (char *) malloc( sizeof (char) * PACKET_LENGTH);
   /*strSequenceChar = (char *) malloc( sizeof (char) * ETH_ALEN);*/
   if((strBufferTemp == NULL) && (strSequenceChar == NULL)) {
      fprintf(stdout,"Memory Allocation Failed\n");
      return 0;
   }
   switch (strBuffer[0]) {
      case ACK_BROADCAST :
         strBufferTemp [0] = strBuffer[0]; 
         /*integerToCharacter (nSequenceNumber, &strSequenceChar);
          */
         for(i = 1; i < 5; i++)
            strBufferTemp[i]=strBuffer[i];
            /*strBufferTemp [i] = strSequenceChar [i-1];*/
         for(j = 5,i = (13+4);strBuffer[i] != '\0'/*i < (int)strlen(strBuffer)*/; i++, j++)
            strBufferTemp [j] = strBuffer [i];
         strBufferTemp [j] = '\0';		
         break;
      case PING:
      case ACK_PING:
        for(i = 0; i < 5; i++)
            strBufferTemp[i]=strBuffer[i];
            /*strBufferTemp [i] = strSequenceChar [i-1];*/
        strBufferTemp[i]=strBufferTemp[i+MAC_ALEN_CHAR];
         for(j = 6,i = (18);strBuffer[i] != '\0'/*i < (int)strlen(strBuffer)*/; i++, j++)
            strBufferTemp [j] = strBuffer [i];
         strBufferTemp [j] = '\0';		
         break;

      default :
         strBufferTemp[0] = BROADCAST;
         /* strBufferTemp[1] = 'U';
            strBufferTemp[2] = 'N';
            strBufferTemp[3] = 'I';
            strBufferTemp[4] = 'Q';
            strBufferTemp[5] = 'U';
            strBufferTemp[6] = 'E'; */
         integerToCharacter (nSequenceNumber, &strSequenceChar);
         /*fprintf(stdout,"SEQ:%s\n",strSequenceChar);*/
         for(i = 1; i < 5; i++)
            strBufferTemp [i] = strSequenceChar [i-1];
         strcpy (strBufferTemp + 5, UNIQUE);
         /*	fprintf(stdout,"CR1:%s\n",strBufferTemp);*/
         /* Source MAC_ID Copying to Broadcast Frame */
         for (j = 0,i = 11; j < MAC_ALEN_CHAR; j++,i++)
            strBufferTemp[i] = strSourceMac[j];
         strBufferTemp[i]='\0';
   free(strSequenceChar);
         break;
   }
   *strReturnBuffer = strBufferTemp;
   return 0;
}

# ifndef __RECEIVER_H_
# define __RECEIVER_H_

/* Function Prototyping */
int checkMacIsPresent(char *strBuffer,char **strMacInfo,int *nIndexOfMacInfo);
int subStringMatchSelfMac (char *strSearch, char *strString);
int packetIdentification (char strBufferReceive [PACKET_LENGTH],char ** strMacInfo, int * nIndexOfMacInfo,FILE *fpWriteFile);
int subStringMatch1 (char *strSearch, char *strString); 
# endif

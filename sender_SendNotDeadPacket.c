#include "network.h"
#include "sender.h"

int sendNotDeadPacket (unsigned int *nSequenceNumber,FILE *fpWriteFile) {
   unsigned long int nTimeLoop;
   unsigned int nSequenceNumberTemp;
   int nNoOfSendsBytes,nSocket_fd_Broadcast=0;
   char strBufferFile[PACKET_LENGTH], *strBufferNotDead; 
   struct sockaddr_ll sockaddr_ll_Broadcast;

   memset (&strBufferFile , 0, (sizeof(char)*PACKET_LENGTH));
   nSocket_fd_Broadcast = socket (AF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL));
   if (nSocket_fd_Broadcast == -1) {
      fprintf (stderr,"%d %s\n",errno,strerror(errno));
      return -1;
   }
   createSockStructure (&sockaddr_ll_Broadcast,nSocket_fd_Broadcast,strBufferFile);
   createNotDeadFrame (&strBufferNotDead,(unsigned int)*nSequenceNumber);
   /* Sending Keep Alive Frame After per 1 min */    
   nNoOfSendsBytes = sendto (nSocket_fd_Broadcast, strBufferNotDead,( size_t)PACKET_LENGTH, 0, (struct sockaddr*)&sockaddr_ll_Broadcast, (socklen_t)sizeof(sockaddr_ll_Broadcast));
   if (nNoOfSendsBytes == -1 ) {
      fprintf (stderr,"%d %s\n",errno,strerror(errno));
      return -1;
   }
   close(nSocket_fd_Broadcast);
   fprintf(stdout,"ND seq no:%d\n",*nSequenceNumber);
   free(strBufferNotDead);
   nSequenceNumberTemp=(*nSequenceNumber)+1;

   for (nTimeLoop = 0 ;nTimeLoop < /*62*/ TIME_ONE_MIN ; ) {
      sendBroadcastPacket(&nSequenceNumberTemp,fpWriteFile);
      nTimeLoop+=TIME_TEN_SEC;
   }
   *nSequenceNumber=nSequenceNumberTemp;
   return 0;
}

# include "network.h"
#include "sender.h"

int takeActionDependOnFile(unsigned int *nSequenceNumber) {
   int nSocket_fd_Ack,nNoOfSendsBytes;
   unsigned long int nTimeLoop1;
   unsigned int nSequenceNumberTemp;
   char *strBufferAck;
   char strSourceMac [MAC_ALEN_CHAR] = SELF_MAC_ID, strBufferFile[PACKET_LENGTH];
   FILE *fpReadFile;
   struct sockaddr_ll sockaddr_ll_Ack;
   struct stat pStat;

   memset (&strBufferFile , 0, (sizeof(char)*PACKET_LENGTH));
   nSequenceNumberTemp=(unsigned int)*nSequenceNumber;

   for (nTimeLoop1 = 0; nTimeLoop1 < /*10 */ TIME_TEN_SEC ; nTimeLoop1++) {      
      fpReadFile = fopen ("ReceiverInfo.txt","r");
      if (fpReadFile != NULL) {
         fileInfo("ReceiverInfo.txt");
         fgets (strBufferFile, PACKET_LENGTH, fpReadFile);
         if (strBufferFile [0] == ACK_BROADCAST || strBufferFile[0]==PING || strBufferFile[0]==ACK_PING) {    
            nSocket_fd_Ack = socket (AF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL));
            if (nSocket_fd_Ack == -1) {
               fprintf (stderr,"%d %s\n",errno,strerror(errno));
               return 0;
            }
            createSockStructure (&sockaddr_ll_Ack,nSocket_fd_Ack,strBufferFile);
            createFrameBuffer (&strBufferAck,strBufferFile,strSourceMac,nSequenceNumberTemp);
            nNoOfSendsBytes = sendto (nSocket_fd_Ack, strBufferAck, (size_t)PACKET_LENGTH, 0, (struct sockaddr*)&sockaddr_ll_Ack, (socklen_t)sizeof(sockaddr_ll_Ack));
            if (nNoOfSendsBytes == -1) {
               fprintf(stderr,"%s %d\n",strerror(errno),errno);
               return 0;
            }
            fprintf(stdout,"Packet Type %d seq no:%d\n",(int)strBufferAck[0],nSequenceNumberTemp);
            free(strBufferAck);
            nSequenceNumberTemp++;
         }
         /*fclose (fpReadFile);*/
         fflush(fpReadFile);
         system (DELETE_ONE_LINE("ReceiverInfo.txt"));
         fileInfo("ReceiverInfo.txt");
         stat ("ReceiverInfo.txt",&pStat);
         if (pStat.st_size == 0) {
            unlink ("ReceiverInfo.txt");
         }
      }
   }
   *nSequenceNumber=nSequenceNumberTemp;
   return 0;
}

#include "network.h"
int decidePath (char *strMacInfo, char *strSearchString, char **strDestination, int *nHopCount) {
   int nIndexOfMacInfoTemp=0,nLengthMacInfo,nIndexOfTempMac,nHopCountTemp=0,nLengthCompare=0;
   char strTempMac[PACKET_LENGTH],*strDestinationTemp;

   nLengthMacInfo = (int)strlen (strMacInfo);
   if ( (*nHopCount ==  0) && (nLengthMacInfo>(MAC_ALEN_CHAR+4)) ) {
      while (nIndexOfMacInfoTemp < nLengthMacInfo) {/* for no. of entries in MacInfo */
         nIndexOfTempMac = 0;
         while (strMacInfo [nIndexOfMacInfoTemp] != '#') /* retrive 1 entry from MacInfo */
            strTempMac[nIndexOfTempMac++] = strMacInfo[nIndexOfMacInfoTemp++];
         nIndexOfMacInfoTemp++;
         strTempMac[nIndexOfTempMac] = '\0';
         if (subStringMatch (strSearchString,strTempMac,&strDestinationTemp,&nHopCountTemp) == 0)  {
            *nHopCount = nHopCountTemp;
            *strDestination=strDestinationTemp;
            return 0;
         }
      }
   }
   else {
      nLengthMacInfo=((*nHopCount+1)*MAC_ALEN_CHAR+*nHopCount);
CHECK_AGAIN:
      while (nIndexOfMacInfoTemp < nLengthMacInfo) {/* for no. of entries in MacInfo */
         for(nIndexOfTempMac=0;strMacInfo [nIndexOfMacInfoTemp]!='#';nIndexOfMacInfoTemp++,nIndexOfTempMac++)
            strTempMac[nIndexOfTempMac++] = strMacInfo[nIndexOfMacInfoTemp++];
         nIndexOfMacInfoTemp++;
         strTempMac[nIndexOfTempMac] = '\0';
            if (nLengthCompare!=nIndexOfTempMac) 
               goto CHECK_AGAIN;
         if (subStringMatch (strSearchString,strTempMac,&strDestinationTemp,&nHopCountTemp) == 0)  {
            *nHopCount = nHopCountTemp;
            *strDestination=strDestinationTemp;
            return 0;
         }
      }

   }
   return -1;
}

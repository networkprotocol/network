# include "network.h"
#include "receiver.h"

int packetIdentification (char strBufferReceive [PACKET_LENGTH],char ** strMacInfo, int * nIndexOfMacInfo,FILE *fpWrite) {	
   unsigned int nSequenceNumber;
   int i=0,j=0,nIndexOfTempMac = 0,  nIndexOfMacInfoTemp = 0, nIndexOfBufferReceive = 5, nIndexOfMacInfoTemp1 = 0, nIndexOfMacInfoTemp2 = 0,nHopCount=0, nIndexOfMacInfoPacket = *nIndexOfMacInfo ;	
   char *strNetworkName,*strCheckMac,*strTempMac,*strSenderMac , *strMacInfoTemp,*strDestination;
   void **strFreeMem,**strFreeMemStore;
   FILE *fpWriteFile;
   struct timeval pTime;

   characterToInteger(strBufferReceive,&nSequenceNumber,&strFreeMem);
   fprintf(stdout,"Seq NO:%u\n",nSequenceNumber);
   strFreeMemStore = (void **) malloc(sizeof(void *)*5);
   for(i=0;i<5;i++)
      strFreeMemStore[i]=strFreeMem[i];
   strMacInfoTemp = (char *) malloc (sizeof (char) *ONEMB);
   memset(strMacInfoTemp,0,ONEMB);
   strcpy (strMacInfoTemp, *strMacInfo);
   free(*strMacInfo);
   for(i=0;i<5;i++)
      free(strFreeMemStore[i]);
   free(strFreeMemStore);
   strSenderMac = (char *) malloc (sizeof(char) * (MAC_ALEN_CHAR+1));
   strCheckMac = (char *) malloc (sizeof (char) * PACKET_LENGTH);
   strTempMac = (char *) malloc (sizeof (char) * PACKET_LENGTH);
   strNetworkName = (char *) malloc (sizeof(char) * 8);
   if (((strSenderMac == NULL) && (strCheckMac == NULL) && (strTempMac == NULL) && (strNetworkName == NULL) &&(strMacInfoTemp==NULL)))
   { fprintf(stdout,"Memory Allocation Error\n"); return -1;}
   switch (strBufferReceive [0]) {
      case BROADCAST :
         for(i = 0; i < ETH_ALEN; i++, nIndexOfBufferReceive++)
            strNetworkName [i] = strBufferReceive[nIndexOfBufferReceive];
         strNetworkName [i] = '\0';
         for (j = 0;j < MAC_ALEN_CHAR; j++, nIndexOfBufferReceive++)
            strSenderMac [j] = strBufferReceive [nIndexOfBufferReceive];
         strSenderMac [j] = '\0';
         fprintf(stdout,"%s %s\n",strNetworkName,strSenderMac);
         if (((strcmp (strNetworkName, UNIQUE) == 0) &&  !(strcmp (strSenderMac, SELF_MAC_ID) == 0))) {
            fileInfo("ReceiverInfo.txt");
            fpWriteFile=createFile("ReceiverInfo.txt");
            fputc (ACK_BROADCAST, fpWriteFile);
            for (i=1;i<5;i++)
               fputc (strBufferReceive[i],fpWriteFile);
            fputs (strSenderMac, fpWriteFile);				 
            fputs (strMacInfoTemp, fpWriteFile);
            fflush(fpWriteFile);
            fileInfo("ReceiverInfo.txt");
         }

         break ;
      case ACK_BROADCAST :
         for (i=5,j=0;i<17;j++,i++) 
            strSenderMac[j]=strBufferReceive[i];
         strSenderMac[j]='\0';
         if (!strcmp(strSenderMac,SELF_MAC_ID) == 0) {
            checkMacIsPresent (strBufferReceive, &strMacInfoTemp, &nIndexOfMacInfoPacket);
            gettimeofday(&pTime,NULL);
            fprintf(fpWrite,"Seq No:%u %d %d\n",nSequenceNumber,(int)pTime.tv_sec,(int)pTime.tv_usec);
            fflush(fpWrite);
            fprintf(stdout,"%s\n",strMacInfoTemp);
         }
         break;
      case NOT_DEAD :
         nIndexOfBufferReceive = 5;
         for(i = 0; i < MAC_ALEN_CHAR; i++, nIndexOfBufferReceive++)
            strSenderMac [i] = strBufferReceive [nIndexOfBufferReceive];
         strSenderMac [i] = '\0';

         if (!(strcmp(strSenderMac,SELF_MAC_ID) == 0))
         {
            fpWriteFile = createFile("TIME_INFO.txt");
            fputs (strSenderMac,fpWriteFile);
            fflush(fpWriteFile);
         }
         break;
      case DELETE_MAC :
         nIndexOfBufferReceive = 5; 
         nIndexOfMacInfoTemp = 0;
         for(i = 0; i < MAC_ALEN_CHAR; i++, nIndexOfBufferReceive++)
            strSenderMac [i] = strBufferReceive [nIndexOfBufferReceive];
         strSenderMac [i] = '\0';
         strcpy (strCheckMac,SELF_MAC_ID);
         strCheckMac [MAC_ALEN_CHAR] = '-';
         strcpy (strCheckMac + MAC_ALEN_CHAR + 1, strSenderMac);
         while (nIndexOfMacInfoTemp < (int)strlen(strMacInfoTemp)) /* for no. of entries in MacInfo */
         {
            nIndexOfTempMac = 0;
            while (strMacInfoTemp [nIndexOfMacInfoTemp] != '#') /* retrive 1 entry from MacInfo */
               strTempMac [nIndexOfTempMac++] = strMacInfoTemp [nIndexOfMacInfoTemp++];
            nIndexOfMacInfoTemp++;
            strTempMac [nIndexOfTempMac] = '\0';
            if (subStringMatch1(strCheckMac,strTempMac) == 0 ) {
               nIndexOfMacInfoTemp1 = nIndexOfMacInfoTemp - ((int)strlen(strTempMac) + 1 ); 
               nIndexOfMacInfoTemp2 = nIndexOfMacInfoTemp ;
               nIndexOfMacInfoTemp = nIndexOfMacInfoTemp1;
               strcpy(strMacInfoTemp+nIndexOfMacInfoTemp1,strMacInfoTemp+nIndexOfMacInfoTemp2);
               /*while(nIndexOfMacInfoTemp2 < nLengthMacInfo )	
                  strMacInfoTemp [nIndexOfMacInfoTemp1++] = strMacInfoTemp [nIndexOfMacInfoTemp2++];
               memset (strMacInfoTemp + ( (int)strlen(strMacInfoTemp) - ((int) strlen (strTempMac) + 1) ),0,((int) strlen (strTempMac) + 1)); */
            }
         }
         nIndexOfMacInfoPacket = (int) strlen (strMacInfoTemp);
         fprintf (stdout,"%s",strMacInfoTemp);
         break;
      case PROCESS_PING:
         nIndexOfBufferReceive = 5;
         for(i = 0; i < MAC_ALEN_CHAR; i++, nIndexOfBufferReceive++)
            strSenderMac [i] = strBufferReceive [nIndexOfBufferReceive];
         strSenderMac [i] = '\0';
         if(decidePath (strMacInfoTemp,strSenderMac,&strDestination,&nHopCount)==0) {
            fpWriteFile=createFile("ReceiverInfo.txt");
            fputc(PING,fpWriteFile);
            for (i=1;i<5;i++)
               fputc (strBufferReceive[i],fpWriteFile);
            fputs(strDestination,fpWriteFile);
            fputc((char)nHopCount,fpWriteFile);
            fputs(SELF_MAC_ID,fpWriteFile);
            fputs(strSenderMac,fpWriteFile);
            fflush(fpWriteFile);
         }

         break;
      case PING:
         nIndexOfBufferReceive=6+MAC_ALEN_CHAR;
         for(i = 0; i < MAC_ALEN_CHAR; i++, nIndexOfBufferReceive++)
            strSenderMac [i] = strBufferReceive [nIndexOfBufferReceive];
         strSenderMac [i] = '\0';
         fprintf(stdout,"PING:%s\n",strSenderMac);
         if(stringMatch(strSenderMac,SELF_MAC_ID,16)==0) {
            nIndexOfBufferReceive=6;
            for(i = 0; i < MAC_ALEN_CHAR; i++, nIndexOfBufferReceive++)
               strSenderMac [i] = strBufferReceive [nIndexOfBufferReceive];
            strSenderMac [i] = '\0';
            fprintf(stdout,"Inside self DEST:%s\n",strSenderMac);
            if(decidePath (strMacInfoTemp,strSenderMac,&strDestination,&nHopCount)==0) {
               fpWriteFile=createFile("ReceiverInfo.txt");
               fputc(ACK_PING,fpWriteFile);
               for (i=1;i<5;i++)
                  fputc (strBufferReceive[i],fpWriteFile);
               fprintf(stdout,"Intermediagte:%s\n",strDestination);
               fputs(strDestination,fpWriteFile);
               fputc((char)nHopCount,fpWriteFile);
               fputs(SELF_MAC_ID,fpWriteFile);
               fputs(strSenderMac,fpWriteFile);
               fflush(fpWriteFile);
            }
         }
         else {
            nHopCount=(int)strBufferReceive[5];
            if(decidePath (strMacInfoTemp,strSenderMac,&strDestination,&nHopCount)==0) {
               fpWriteFile=createFile("ReceiverInfo.txt");
               fputc(PING,fpWriteFile);
               for (i=1;i<5;i++)
                  fputc (strBufferReceive[i],fpWriteFile);
               fputs(strDestination,fpWriteFile);
               fputc((char)nHopCount,fpWriteFile);
               fputs(SELF_MAC_ID,fpWriteFile);
               fputs(strSenderMac,fpWriteFile);
               fflush(fpWriteFile);
            }
         }
         break;
      case ACK_PING:
         nIndexOfBufferReceive=6+MAC_ALEN_CHAR;
         for(i = 0; i < MAC_ALEN_CHAR; i++, nIndexOfBufferReceive++)
            strSenderMac [i] = strBufferReceive [nIndexOfBufferReceive];
         strSenderMac [i] = '\0';
         fprintf(stdout,"ACK_PING:%s\n",strSenderMac);
         if(stringMatch(strSenderMac,SELF_MAC_ID,16)==0) 
            fprintf(stdout,"PING Successful\n");
         else {
            nHopCount=(int)strBufferReceive[5];
            if(decidePath (strMacInfoTemp,strSenderMac,&strDestination,&nHopCount)==0) {
               fpWriteFile=createFile("ReceiverInfo.txt");
               fputc (ACK_PING,fpWriteFile);
               for (i=1;i<5;i++)
                  fputc (strBufferReceive[i],fpWriteFile);
               fputs(strDestination,fpWriteFile);
               fputc((char)nHopCount,fpWriteFile);
               fputs(SELF_MAC_ID,fpWriteFile);
               fputs(strSenderMac,fpWriteFile);
               fflush(fpWriteFile);
            }
         }

         break;
      default : break;	
   }
   free(strSenderMac);
   free(strCheckMac);
   free(strTempMac);
   free(strNetworkName);
   /*free(strDestination);*/
   *strMacInfo = strMacInfoTemp;
   *nIndexOfMacInfo = nIndexOfMacInfoPacket;
   return 0;
}

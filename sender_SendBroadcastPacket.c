#include "network.h"
#include "sender.h"

int sendBroadcastPacket (unsigned int *nSequenceNumber,FILE *fpWriteFile) {
   int nNoOfSendsBytes,nLimitBroad,nSocket_fd_Broadcast;
   unsigned int nSequenceNumberTemp;
   char *strBufferBroadcast;
   char strSourceMac[MAC_ALEN_CHAR],strBufferFile[PACKET_LENGTH];
   struct sockaddr_ll sockaddr_ll_Broadcast;
   struct timeval pTime;
   strcpy(strSourceMac,SELF_MAC_ID);
   nSocket_fd_Broadcast = socket (AF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL));
   if (nSocket_fd_Broadcast == -1) {
      fprintf (stderr,"%d %s\n",errno,strerror(errno));
      return -1;
   }

   for(nLimitBroad = 0; nLimitBroad < 1; nLimitBroad++) {
      createSockStructure (&sockaddr_ll_Broadcast,nSocket_fd_Broadcast,strBufferFile);
      createFrameBuffer (&strBufferBroadcast,strBufferFile,strSourceMac,(unsigned int)*nSequenceNumber);
      /* Sending Broadcast Frame Every 10 sec */
      gettimeofday(&pTime,NULL);
      fprintf(fpWriteFile,"Seq No:%d %d %d\n",(*nSequenceNumber),(int)pTime.tv_sec,(int)pTime.tv_usec);
      fflush(fpWriteFile);
      /*fclose(fpTemp);*/
      nNoOfSendsBytes = sendto (nSocket_fd_Broadcast, strBufferBroadcast,( size_t)PACKET_LENGTH, 0, (struct sockaddr*)&sockaddr_ll_Broadcast, (socklen_t)sizeof(sockaddr_ll_Broadcast));
      if (nNoOfSendsBytes == -1 ) {
         fprintf (stderr,"%d %s\n",errno,strerror(errno));
         return -1;
      }
      close(nSocket_fd_Broadcast);
      fprintf(stdout,"B seq no:%d\n",*nSequenceNumber);
      free(strBufferBroadcast);
   }
   nSequenceNumberTemp=(*nSequenceNumber)+1;
   takeActionDependOnFile(&nSequenceNumberTemp);
   *nSequenceNumber=nSequenceNumberTemp;
   return 0;
}
